import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MovieListComponent} from './list/list.component';

const routes: Routes = [
  {path:'movie',component:MovieListComponent},
  {path:'show_id/:show_id',component:MovieListComponent},
  {path:'genre_id/:genre_id',component:MovieListComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule { }
