import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MovieRoutingModule } from './movie-routing.module';
import { MovieListComponent } from './list/list.component';

import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';


@NgModule({
  declarations: [MovieListComponent],
  imports: [
    CommonModule,
    MovieRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule
  ]
})
export class MovieModule { }
