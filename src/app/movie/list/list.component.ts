import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';

import { MovieService } from '../../share/service/movie.service';
import { Movie } from '../../share/model/movie.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class MovieListComponent implements OnInit {

  isGenre:boolean=false;
  
  show_id:string="";
  genre_id:string="";
  query_type:string="";
  showtype_name:string="";
  
  limit:string="28";
  columnsLimit:string="4";
  imageUrl:string="http://r7081911.duckdns.org:4404/assets/web/img";
  movies:Movie[]=[];
  
  constructor(private activatedRoute: ActivatedRoute,private movieService:MovieService) { }

  ngOnInit() {
    
    var obsComb = combineLatest(this.activatedRoute.params, this.activatedRoute.url, 
    (params, url) => ({ params, url }));
    
    obsComb.subscribe( ap => {
    console.log(ap.params['show_id']);
    console.log(ap.params['genre_id']); 
    console.log(ap.url[0].path);
    this.show_id=ap.params['show_id'];
    this.genre_id=ap.params['genre_id'];
    this.query_type=ap.url[0].path;
    
    
    this.getMovies();
    });

  }

  onDelete(item:Movie):void{
    console.log(item);
  }
  getMovies(){
    console.log(this.query_type);
      
    if (this.query_type==="show_id"){
        this.movieService.getMoviesByShowType(this.show_id,this.limit).subscribe(movies=>{
        console.log(movies[0].title);
        console.log(movies[0].show_type);
        console.log(movies[0].showtype_name);
        this.showtype_name=movies[0].showtype_name;
        
        this.movies=movies;
        this.isGenre=false;
        
      })} else if (this.query_type==="genre_id") {
        this.movieService.getMoviesByGenreId(this.genre_id,this.limit).subscribe(movies=>{
        this.movies=movies;
        this.isGenre=true;
        console.log(this.movies);
      })} else{
        console.log("bad value"); 
      };
  };

}

