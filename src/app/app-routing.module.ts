import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutUsComponent } from './component/about-us/about-us.component';
import { EnquiryComponent } from './component/enquiry/enquiry.component';
import { HomeComponent } from './component/home/home.component';
import { PathNotFoundComponent } from './component/path-not-found/path-not-found.component';
import { SearchComponent } from './component/search/search.component';

const routes: Routes = [
  
  {path:'about-us',component:AboutUsComponent},
  {path:'enquiry',component:EnquiryComponent},
  {path:'home',component:HomeComponent},
  {path:'movie',loadChildren:'./movie/movie.module#MovieModule'},
  {path:'admin', loadChildren:'./admin/admin.module#AdminModule'},
  {path:'genre', loadChildren:'./genre/genre.module#GenreModule'},
  {path:'showtype',  loadChildren:'./showtype/showtype.module#ShowtypeModule'},
  {path:'path-not-found',component:PathNotFoundComponent},
  {path:'search',component:SearchComponent},
  {path:'',redirectTo:'/home',pathMatch:'full'},
  {path:'**',redirectTo:'/path-not-found',pathMatch:'full'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
