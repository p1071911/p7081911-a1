import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';  
import { ReactiveFormsModule} from '@angular/forms';

import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';

import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutUsComponent } from './component/about-us/about-us.component';
import { HomeComponent } from './component/home/home.component';
import { HeaderNavComponent } from './component/header-nav/header-nav.component';
import { P7081911Component } from './component/p7081911/p7081911.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { HeaderImgComponent } from './component/header-img/header-img.component';
import { EnquiryComponent } from './component/enquiry/enquiry.component';
import { PathNotFoundComponent } from './component/path-not-found/path-not-found.component';
import { HeaderNavMobileComponent } from './component/header-nav-mobile/header-nav-mobile.component';
import { AboutUsHeaderComponent } from './component/about-us/about-us-header/about-us-header.component';
import { AboutUsAsideLeftComponent } from './component/about-us/about-us-aside-left/about-us-aside-left.component';
import { AboutUsAsideRightComponent } from './component/about-us/about-us-aside-right/about-us-aside-right.component';
import { AboutUsMainComponent } from './component/about-us/about-us-main/about-us-main.component';
import { HomeMainComponent } from './component/home/home-main/home-main.component';
import { HomeAsideRightComponent } from './component/home/home-aside-right/home-aside-right.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { SearchComponent } from './component/search/search.component';

import {AdminService} from './share/service/admin.service';

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    HomeComponent,
    HeaderNavComponent,
    P7081911Component,
    HeaderComponent,
    FooterComponent,
    HeaderImgComponent,
    EnquiryComponent,
    PathNotFoundComponent,
    HeaderNavMobileComponent,
    AboutUsHeaderComponent,
    AboutUsAsideLeftComponent,
    AboutUsAsideRightComponent,
    AboutUsMainComponent,
    HomeMainComponent,
    HomeAsideRightComponent,
    HoverHighlightDirective,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatTooltipModule,
    AppRoutingModule
  ],
  providers: [AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
