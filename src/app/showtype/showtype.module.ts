import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowtypeRoutingModule } from './showtype-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowtypeRoutingModule
  ]
})
export class ShowtypeModule { }
