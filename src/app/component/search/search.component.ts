import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MovieService } from '../../share/service/movie.service';
import { GenreService } from '../../share/service/genre.service';
import { Genre } from '../../share/model/genre.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  results:any[];
  searchForm: FormGroup;
  genres:Genre[];
  imageUrl:string="http://r7081911.duckdns.org:4404/assets/web/img";
  
  constructor(private movieService:MovieService,private genreService:GenreService) { }

  ngOnInit() {
  
  this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
      this.genres.push(new Genre("","",""));
    });
    
    
  this.searchForm = new FormGroup({
      'title'   : new FormControl(null, [Validators.required]),
      'genre'  : new FormControl(null, [])
    });
  }
  
  //submit the form
    onSubmit(){
      console.log("component: "+this.searchForm)
      console.log("component: "+this.searchForm.value.title);
      console.log("component: "+this.searchForm.value.genre);
      
      
      if (this.searchForm.value.genre===null){
         this.movieService.getMoviesByTitle(this.searchForm.value.title).subscribe(result=>{
          console.log(result);
          this.results=result;
      });
      
      }else{
        
        var genreId="";
        for (var i=0;i<this.genres.length;i++){
          if (this.genres[i].genre_name==this.searchForm.value.genre){
            genreId=this.genres[i].genre_id;
          }
        }
        
        console.log(genreId);
        
        this.movieService.getMoviesByTitleAndGenre(this.searchForm.value.title,genreId).subscribe(result=>{
          console.log(result);
          this.results=result;
        
      });
    }
}

}
