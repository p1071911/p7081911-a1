import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../share/service/global.service';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-header-nav-mobile',
  templateUrl: './header-nav-mobile.component.html',
  styleUrls: ['./header-nav-mobile.component.css']
})
export class HeaderNavMobileComponent implements OnInit {

  title:string;
  
  constructor(private globalService:GlobalService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.title=this.globalService.getGV().title;
    this.matIconRegistry.addSvgIcon(
      'favicon',
       this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/favicon.svg')
    );
  }

  
  
}
