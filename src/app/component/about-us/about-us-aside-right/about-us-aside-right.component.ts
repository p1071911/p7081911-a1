import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../share/service/global.service';

@Component({
  selector: 'app-about-us-aside-right',
  templateUrl: './about-us-aside-right.component.html',
  styleUrls: ['./about-us-aside-right.component.css']
})
export class AboutUsAsideRightComponent implements OnInit {

  title:string='';
  version:string='';
  telno:string='';
  email:string='';
  address:string='';
  
  constructor(private globalService:GlobalService) { }

  ngOnInit() {
    this.title=this.globalService.getGV().title;
    this.version=this.globalService.getGV().version;
    this.telno=this.globalService.getGV().telno;
    this.email=this.globalService.getGV().email;
    this.address=this.globalService.getGV().address;
  }

}
