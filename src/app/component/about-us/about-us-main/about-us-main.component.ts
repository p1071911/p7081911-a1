import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../share/service/global.service';

@Component({
  selector: 'app-about-us-main',
  templateUrl: './about-us-main.component.html',
  styleUrls: ['./about-us-main.component.css']
})
export class AboutUsMainComponent implements OnInit {

  title:string='';
  constructor(private globalService:GlobalService) { }

  ngOnInit() {
    this.title=this.globalService.getGV().title;
  }

}
