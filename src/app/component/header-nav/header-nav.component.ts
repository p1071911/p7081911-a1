import { Component, OnInit } from '@angular/core';
import {AdminService } from '../../share/service/admin.service';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit {

  constructor(private admin:AdminService) { }

  ngOnInit() {
  }

  onLogout(){
    this.admin.logout();
  }
}
