import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../share/service/movie.service';

@Component({
  selector: 'app-p7081911',
  templateUrl: './p7081911.component.html',
  styleUrls: ['./p7081911.component.css'],
  providers:[MovieService]
})
export class P7081911Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
