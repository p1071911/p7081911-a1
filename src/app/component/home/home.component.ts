import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../share/service/movie.service';
import { Movie } from '../../share/model/movie.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  movies:Movie[]=[];
  movieStr:string="";
  constructor() { }

  ngOnInit() {
   
  }

}
