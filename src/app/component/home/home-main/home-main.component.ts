import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../../share/service/movie.service';
import { Movie } from '../../../share/model/movie.model';
import { AdminService } from '../../../share/service/admin.service';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.css']
})


export class HomeMainComponent implements OnInit {
  
  limit:string="7";
  imageUrl:string="http://r7081911.duckdns.org:4404/assets/web/img";
  currentMovies:Movie[]=[];
  upcomingMovies:Movie[]=[];
  premierMovies:Movie[]=[];
  archivedMovies:Movie[]=[];
  isAuthenticated:boolean=false;
  
  movieStr:string="";
  constructor(private movieService:MovieService,private adminService:AdminService) { }

  ngOnInit() {
    this.isAuthenticated=this.hasValidToken();
  
    this.movieService.getMoviesByShowType("1",this.limit).subscribe(movies=>{
      this.currentMovies=movies;
    });
    this.movieService.getMoviesByShowType("2",this.limit).subscribe(movies=>{
      this.upcomingMovies=movies;
    });
    this.movieService.getMoviesByShowType("3",this.limit).subscribe(movies=>{
      this.premierMovies=movies;
    });
    this.movieService.getMoviesByShowType("4",this.limit).subscribe(movies=>{
      this.archivedMovies=movies;
    });
 
    
  }
  
  
  //if token is valid, set admin.loggedIn to true
  //else clear local storage jwt and session, if token expired.
  
  hasValidToken(){

// exp: 1561243704
// iat: 1561157304
// name: "Alvin"
// role: "admin"

    var currentTime = Date.now() / 1000;
    console.log(currentTime);
    
    var validToken=false;
    var result:any;
    var jSession:any;
    var isAuthenticated=false;
    
    if ((localStorage.getItem("session") !== null)){
      var session=localStorage.getItem('session');
      console.log("\'"+session+"\'");
     
      jSession=JSON.parse(session);
      console.log("component "+jSession);
      console.log("component "+jSession.exp);
      console.log("component "+jSession.name);
      console.log("component "+jSession.role);
      
      if (Number(jSession.exp)>currentTime){
        isAuthenticated=true;
        this.adminService.isAuthenticated();
      }else{
        console.log("component: token expired: currentTime:" + currentTime.toString()+" exp:"+jSession.exp);
        //clear localstorage
        localStorage.setItem("session","");
      }
    }
    
    if ((localStorage.getItem("jwt") !== null)){
      var jwt=localStorage.getItem('jwt');
      result=this.adminService.getDecodedAccessToken(jwt);
      
      if (result.exp>currentTime){
        console.log("validToken");
        localStorage.setItem("session", `{"exp":"${result.exp}","name":"${result.name}","role":"${result.role}"}`);
        isAuthenticated=true;
        this.adminService.isAuthenticated();
      }else{
        console.log("token expired");
        localStorage.setItem("jwt","");
      }
    }else{
      console.log("no token");
    }
    
    return isAuthenticated;
  }
  
  OnMatCardClickEvent(item:Movie): void {
    // this.movieSelected.emit(item);
    // console.log(item);
  }

}





