export class Movie{
    public movie_id:string;
    public title:string;
    public actors:string;
    public studio:string;
    public year:string;
    public genre_name:string;
    public genre_description:string;
    public image_url:string;
    public show_type:string;
    public popularity:string;
    public overview:string;
    public showtype_name:string;
    
    
    constructor(movie_id:string,title:string,actors:string,studio:string,year:string,genre_name:string,genre_description:string,image_url:string,show_type:string,popularity:string,overview:string,showtype_name:string)

    {this.movie_id=movie_id;
    this.title=title;
    this.actors=actors;
    this.studio=studio;
    this.year=year;
    this.genre_name=genre_name;
    this.genre_description=genre_description;
    this.image_url=image_url;
    this.show_type=show_type;
    this.popularity=popularity;
    this.overview=overview;
    this.showtype_name=showtype_name;
    }
}

