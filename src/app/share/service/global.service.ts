import { Injectable } from '@angular/core';
import {Global} from '../model/global.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  myGV:Global = new Global('SP Theatre','0.0.1','500 Dover Rd, S139651','6772 1900','enquiry@sptheatre.sp.sg');
  
  constructor() { }
  
  getGV():Global{
      // console.log(this.myGV);
      return this.myGV;
    }
}
