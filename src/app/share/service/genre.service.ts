import { Injectable } from '@angular/core';
import { Genre } from '../model/genre.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class GenreService {

genresUrl:string= "http://p7081911.duckdns.org:3000/";
genreStr:string="";
genres:Genre[]=[];

  constructor(private httpClient:HttpClient) { }
  
  getGenres(){
    this.genreStr=this.genresUrl+"api/genre";
    return this.httpClient.get<Genre[]>(`${this.genreStr}`)
      .pipe(
        map((genres)=>{
          this.genres=genres;
          console.log(this.genres);
          return genres;
        },
        (error)=>{console.log(error);}
        )
      );
  }
  
  // /api/movie/:movie_id
  deleteGenre(genre_id:string,jwt:string){
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    
    this.genreStr=this.genresUrl+"api/genre/"+genre_id;
    
    return this.httpClient.delete<void>(`${this.genreStr}`,options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
    );
  }
  
  addGenre(newItem:Genre,jwt:string){
    
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    
    this.genreStr=this.genresUrl+"api/genre";
    console.log("service:"+ newItem);
    console.log("service:"+ newItem.genre_id);
    console.log("service:"+ newItem.genre_name);
    console.log("service:"+ newItem.genre_description);
    
    
    return this.httpClient.post<void>(`${this.genreStr}`,{info:newItem},options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
    );
  };
  
  //update show type
  updateGenre(newItem:Genre,jwt:string){
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
      
    });
    const options={
      headers:httpHeaders
    }
    
    this.genreStr=this.genresUrl+"api/genre/"+newItem.genre_id;
    console.log(this.genreStr);
    console.log("service:"+ newItem.genre_id);
    console.log("service:"+ newItem.genre_name);
    console.log("service:"+ newItem.genre_description);
    return this.httpClient.put<void>(`${this.genreStr}`,{info:newItem},options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
    );
    
  };
  
  
}
