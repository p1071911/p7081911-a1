import { Injectable } from '@angular/core';
import { Admin } from '../model/admin.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import * as jwt_decode from "jwt-decode";


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  loggedIn:boolean=false;
  
  adminsUrl:string= "http://p7081911.duckdns.org:3000/";
  adminStr:string="";
  admins:Admin[]=[];

  constructor(private httpClient:HttpClient) { }
  
  isAuthenticated(){
    this.loggedIn=true;
  }
  
  logout(){
    this.loggedIn=false;
  }
  
  
  
  
    //log in to server, if valid return a jwt
    login(login:Admin){
      const httpHeaders= new HttpHeaders({
        'Content-Type':'application/json',
        'Cache-Control': 'no-cache'
      });
      const options={
        headers:httpHeaders
      };
      
      console.log("service: " + login.name + " "+login.password);
      this.adminStr=this.adminsUrl+"api/login";
      return this.httpClient.post<void>(`${this.adminStr}`,{info:login},options)
        .pipe(
          (error)=>{
            console.log(error);
            return error;
          }
        )
    };
    
    
    //decode jwt string
    //jwt after decode will return exp, name and role
    getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        console.log(Error);
        return null;
    }
  }
  
}
  

 

  

