import { Injectable } from '@angular/core';
import { Showtype } from '../model/showtype.model';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {catchError, map } from 'rxjs/operators';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShowtypeService {


showtypesUrl:string= "http://p7081911.duckdns.org:3000/";
showtypeStr:string="";
showtypes:Showtype[]=[];

  constructor(private httpClient:HttpClient) { }
  

  
  getShowtypes(){
    this.showtypeStr=this.showtypesUrl+"api/showtype";
    return this.httpClient.get<Showtype[]>(`${this.showtypeStr}`)
    .pipe(
      map((showtypes)=>{
        this.showtypes=showtypes;
        console.log(this.showtypes);
        return showtypes;
      }),
      (error) =>{
        console.log(error);
        return error;
      }
     );
  };
  
  addShowtype(newItem:Showtype,jwt:string){
    
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    
    this.showtypeStr=this.showtypesUrl+"api/showtype";
    console.log("service:"+ newItem);
    return this.httpClient.post<void>(`${this.showtypeStr}`,{info:newItem},options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
     );
  };
  
  
  // /api/movie/:movie_id
  deleteShowtype(id:string,jwt:string){
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    }
    
    this.showtypeStr=this.showtypesUrl+"api/showtype/"+id;
    return this.httpClient.delete<void>(`${this.showtypeStr}`,options);
  };
  
  //update show type
  updateShowtype(newItem:Showtype,jwt:string){
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    }
    
    this.showtypeStr=this.showtypesUrl+"api/showtype/"+newItem.showtype_id;
    console.log(this.showtypeStr);
    console.log("service:"+ newItem);
    return this.httpClient.put<void>(`${this.showtypeStr}`,{info:newItem},options);
  };
  
}
