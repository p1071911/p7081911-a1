import { Injectable } from '@angular/core';
import { Movie } from '../model/movie.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


  
  
@Injectable({
  providedIn: 'root'
})
export class MovieService {

// moviesUrl:string= "http://r7081911.duckdns.org:3000/m/m5.json";
moviesUrl:string= "http://p7081911.duckdns.org:3000/";
movieStr:string="";
movies:Movie[]=[]

  moviesLimit:string ="?_limit=";
  
  constructor(private httpClient:HttpClient) { }
  
  getMoviesByTitle(title:string){
    // console.log(this.movies);
    // return this.movies.slice();
    this.movieStr=this.moviesUrl+"api/movie/"+title;
    return this.httpClient.get<Movie[]>(`${this.movieStr}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        return movies;
      },
      (error)=>{console.log(error);}
      )
    );
  }
  
   getMoviesByTitleAndGenre(title:string,genreId:string){
    // console.log(this.movies);
    // return this.movies.slice();
    this.movieStr=this.moviesUrl+"api/movie/"+genreId+"/"+title;
    return this.httpClient.get<Movie[]>(`${this.movieStr}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        return movies;
      },
      (error)=>{console.log(error);}
      )
    );
  }
  getMoviesByShowType(show_type:string,_limit:string){
    // console.log(this.movies);
    // return this.movies.slice();
    this.movieStr=this.moviesUrl+"api/show_id/"+show_type+"/movie";
    return this.httpClient.get<Movie[]>(`${this.movieStr}${this.moviesLimit}${_limit}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        return movies;
      },
      (error)=>{console.log(error);}
      )
    );
  }
  
  getMoviesByGenreId(genre_id:string,_limit:string){
    // console.log(this.movies);
    // return this.movies.slice();
    this.movieStr=this.moviesUrl+"api/genre_id/"+genre_id+"/movie";
    return this.httpClient.get<Movie[]>(`${this.movieStr}${this.moviesLimit}${_limit}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        return movies;
      },
      (error)=>{console.log(error);}
      )
    );
  }
  
  // /api/movie/:movie_id
  deleteMovie(movie_id:string,jwt:string){
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    this.movieStr=this.moviesUrl+"api/movie/"+movie_id;
    return this.httpClient.delete<void>(`${this.movieStr}`,options);
  }
  
  getMovies(_limit:string){
    // console.log(this.movies);
    // return this.movies.slice();
    this.movieStr=this.moviesUrl+"api/movie";
    return this.httpClient.get<Movie[]>(`${this.movieStr}${this.moviesLimit}${_limit}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        return movies;
      },
      (error)=>{console.log(error);}
      )
    );
  }

  loadMovies(){
    return this.httpClient.get<Movie[]>(`${this.moviesUrl}`)
    .pipe(
      map((movies)=>{
        this.movies=movies;
        console.log(movies);
        return movies;
      },
      (error)=>{console.log(error);}
      )
      );
  }
  
  addMovie(newItem:Movie,jwt:string){
    
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    
    this.movieStr=this.moviesUrl+"api/movie";
    console.log("service:"+ newItem);
    console.log("service:"+ newItem.movie_id);
    console.log("service:"+ newItem.title);
    console.log("service:"+ newItem.genre_name); //captured the genre_ id
    console.log("service:"+ newItem.showtype_name); //captured the showtype_id
    console.log("service:"+ newItem.year);
    
    return this.httpClient.post<void>(`${this.movieStr}`,{info:newItem},options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
    );
  };
  
  
  updateMovie(updatedItem:Movie,jwt:string){
    
    const httpHeaders= new HttpHeaders({
      'Content-Type':'application/json',
      'Cache-Control': 'no-cache',
      'Authorization':'Bearer '+jwt
    });
    const options={
      headers:httpHeaders
    };
    
    this.movieStr=this.moviesUrl+"api/movie/"+updatedItem.movie_id;
    console.log("service:"+ updatedItem);
    console.log("service:"+ updatedItem.movie_id);
    console.log("service:"+ updatedItem.title);
    console.log("service:"+ updatedItem.genre_name); //captured the genre_ id
    console.log("service:"+ updatedItem.showtype_name); //captured the showtype_id
    console.log("service:"+ updatedItem.year);
    
    return this.httpClient.put<void>(`${this.movieStr}`,{info:updatedItem},options)
    .pipe(
      (error) =>{
        console.log(error);
        return error;
      }
    );
  };
  
}
