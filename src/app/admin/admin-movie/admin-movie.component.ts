import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../share/service/movie.service';
import { Movie } from '../../share/model/movie.model';
import { GenreService } from '../../share/service/genre.service';
import { Genre } from '../../share/model/genre.model';
import { ShowtypeService } from '../../share/service/showtype.service';
import { Showtype } from '../../share/model/showtype.model';
import { FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-admin-movie',
  templateUrl: './admin-movie.component.html',
  styleUrls: ['./admin-movie.component.css']
})
export class AdminMovieComponent implements OnInit {
  
  toUpdate:boolean=false;
  toListAll:boolean=true;
  toUpdateMovie:Movie;
  
  inputInfo:Movie=new Movie("","","","","","","","","","","","");
  
  eventMessage:string="";
  limit:string="50";
  movies:Movie[]=[];
  genres:Genre[]=[];
  showtypes:Showtype[]=[];
  
  displayedColumns: string[] = ['id','title','genre','showtype','year','actions'];
  updateForm:FormGroup;
  jwt=localStorage.getItem("jwt");
  
  constructor(private movieService:MovieService,private genreService:GenreService,private showtypeService:ShowtypeService) { }

  ngOnInit() {
    this.movieService.getMovies(this.limit).subscribe(movies=>{
      this.movies=movies;
      console.log(this.movies);
    });
  
    
    this.updateForm = new FormGroup({
      'title'   : new FormControl(null, [Validators.required,this.blankSpaces]),
      'genre'   : new FormControl(null, []),
      'showtype'  : new FormControl(null, []),
      'year'  : new FormControl(null, [])
    });
    }
  
  
    blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }
    
  
  
  onUpdate(item:Movie){
    
    this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
      console.log(this.genres);
    });
    this.showtypeService.getShowtypes().subscribe(showtypes=>{
      this.showtypes=showtypes;
      // console.log(this.genres);
      console.log(item);
      this.toUpdate=true;
      this.toUpdateMovie=item;
      this.eventMessage="";
      
      this.toUpdateMovie=item;  
      this.inputInfo.title=item.title;
      this.inputInfo.genre_name=item.genre_name;
      this.inputInfo.showtype_name=item.showtype_name;
      this.inputInfo.year=item.year;
      this.inputInfo.movie_id=item.movie_id;
      
      console.log("component: "+this.inputInfo.movie_id+" "+this.inputInfo.title+" "+this.inputInfo.genre_name+" "+this.inputInfo.showtype_name+" "+this.inputInfo.year);
      
    });
    
    
    
  }
  
  onDelete(item:Movie){
    console.log(item);
    this.movieService.deleteMovie(item.movie_id.toString(),this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      
      this.eventMessage=result["message"];
      this.toUpdate=false;
      
      this.movieService.getMovies(this.limit).subscribe(movies=>{
      this.movies=movies;
      console.log(this.movies);
    });
    });
  }
  
  updateCancel(){
    this.toUpdate=false;
    this.eventMessage="Update Cancel";
  }
  updateSubmit(){
    this.toUpdate=false;
    this.eventMessage="Update Submit";
    console.log(this.updateForm);
    console.log("updateSubmit: "+this.inputInfo.movie_id +" "+this.updateForm.value.title +
    " "+this.updateForm.value.genre + " "+this.updateForm.value.showtype+ " "+this.updateForm.value.year);
    
    
    this.inputInfo.year=this.updateForm.value.year
    this.inputInfo.title=this.updateForm.value.title;
    this.inputInfo.genre_name=this.updateForm.value.genre; //pass in the id into genre name
    this.inputInfo.showtype_name=this.updateForm.value.showtype; //pass in the id into showtype name
      
    this.movieService.updateMovie(this.inputInfo,this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      this.eventMessage=result["message"];
      
      this.movieService.getMovies(this.limit).subscribe(movies=>{
      this.movies=movies;
    });
    });
    
  }
  
  onNewItemAdded(eventMessage:string){
    this.eventMessage=eventMessage;
    this.movieService.getMovies(this.limit).subscribe(movies=>{
        this.movies=movies;
        this.toListAll=true;
        this.toUpdate=false;
      });
  }
  
  onNewItemAdding(eventMessage:string){
    this.eventMessage=eventMessage;
    this.toUpdate=false;
    this.toListAll=false;
  }
  
  onNewItemCancel(eventMessage:string){
    this.eventMessage=eventMessage;
    this.toUpdate=false;
    this.toListAll=true;
  }
  

}
