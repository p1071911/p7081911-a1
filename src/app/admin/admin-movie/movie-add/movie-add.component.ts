import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MovieService } from '../../../share/service/movie.service';
import { Movie } from '../../../share/model/movie.model';
import { GenreService } from '../../../share/service/genre.service';
import { Genre } from '../../../share/model/genre.model';
import { ShowtypeService } from '../../../share/service/showtype.service';
import { Showtype } from '../../../share/model/showtype.model';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  
  @Output() newItemCancel= new EventEmitter<string>();
  @Output() newItemAdded= new EventEmitter<string>();
  @Output() newItemAdding= new EventEmitter<string>();
  
  genres:Genre[];
  showtypes:Showtype[];
  
  title:string="";
  eventMessage:string="";
  addForm: FormGroup;
  toShowAddForm:boolean;
  inputInfo:Movie=new Movie("","","","","","","","","","","","");
 
  jwt=localStorage.getItem("jwt");
    
  
  constructor(private movieService:MovieService,private genreService:GenreService,private showtypeService:ShowtypeService) { }

  ngOnInit() {
    this.title="Movie";
    this.toShowAddForm=false;
    
    this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
    });
    this.showtypeService.getShowtypes().subscribe(showtypes=>{
      this.showtypes=showtypes;
    });
    
    
    this.addForm = new FormGroup({
      'title'   : new FormControl(null, [Validators.required]),
      'genre'  : new FormControl(null, []),
      'showtype'  : new FormControl(null, []),
      'year'  : new FormControl(null, [])
    });
    
  }
    
    
    //to display the addform
    add(){
      this.newItemAdding.emit("");
      this.inputInfo=new Movie("","","","","","","","","","","","");
      this.toShowAddForm=true;
    }
    
    //add cancel, hid the add form
    addCancel(){
      this.newItemCancel.emit("Update Cancel");
      this.toShowAddForm=false;
    }
    
    //submit the form
    onSubmit(){
      console.log("component: "+this.addForm)
      console.log("component: "+this.addForm.value.title);
      console.log("component: "+this.addForm.value.genre);
      console.log("component: "+this.addForm.value.showtype);
      console.log("component: "+this.addForm.value.year);
      
      this.inputInfo.title=this.addForm.value.title
      this.inputInfo.genre_name=this.addForm.value.genre;
      this.inputInfo.showtype_name=this.addForm.value.showtype;
      this.inputInfo.year=this.addForm.value.year;
      
      console.log("component: "+this.inputInfo);
      
      this.movieService.addMovie(this.inputInfo,this.jwt).subscribe(result=>{
        console.log(result);
        console.log(result["message"]);
        console.log(result["result"]);
        
        if (result["result"]==="OK"){
          this.eventMessage=result["message"];
        }else if(result["result"]==="ERROR"){
          this.eventMessage=result["message"];
        }else{
          this.eventMessage=result["message"];
        }
        
        this.newItemAdded.emit(this.eventMessage);
      });
      this.toShowAddForm=false;
    };
  
}
