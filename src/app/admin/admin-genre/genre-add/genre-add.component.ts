import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GenreService } from '../../../share/service/genre.service';
import { Genre } from '../../../share/model/genre.model';

@Component({
  selector: 'app-genre-add',
  templateUrl: './genre-add.component.html',
  styleUrls: ['./genre-add.component.css']
})


 
  
  
export class GenreAddComponent implements OnInit {

  @Output() newItemCancel= new EventEmitter<string>();
  @Output() newItemAdded= new EventEmitter<string>();
  @Output() newItemAdding= new EventEmitter<string>();
  
  title:string="";
  eventMessage:string="";
  addForm: FormGroup;
  toShowAddForm:boolean;
  inputInfo:Genre=new Genre("","","");
  jwt=localStorage.getItem("jwt");
  
  constructor(private genreService:GenreService) { }

  ngOnInit() {
    this.title="Genre";
    this.toShowAddForm=false;
    
    this.addForm = new FormGroup({
      'name'   : new FormControl(null, [Validators.required, this.blankSpaces]),
      'description'  : new FormControl(null, [])
    });
  }
  
    blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }
  
    //to display the addform
    add(){
      this.newItemAdding.emit("");
      this.inputInfo=new Genre("","","");
      this.toShowAddForm=true;
    }
    
    //add cancel, hid the add form
    addCancel(){
      this.newItemCancel.emit("Update Cancel");
      this.toShowAddForm=false;
    }
    
    //submit the form
    onSubmit(){
      console.log("component: "+this.addForm)
      console.log("component: "+this.addForm.value.name);
      console.log("component: "+this.addForm.value.description);
      
      this.inputInfo.genre_name=this.addForm.value.name
      this.inputInfo.genre_description=this.addForm.value.description;
      
      console.log("component: "+this.inputInfo);
      this.genreService.addGenre(this.inputInfo,this.jwt).subscribe(result=>{
        console.log(result);
        console.log(result["message"]);
        console.log(result["result"]);
        
        if (result["result"]==="OK"){
          this.eventMessage=result["message"];
        }else if(result["result"]==="ERROR"){
          this.eventMessage=result["message"];
        }else{
          this.eventMessage=result["message"];
        }
        
        this.newItemAdded.emit(this.eventMessage);
      });
      this.toShowAddForm=false;
    };
}

