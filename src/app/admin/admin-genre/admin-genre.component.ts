import { Component, OnInit } from '@angular/core';
import { GenreService } from '../../share/service/genre.service';
import { Genre } from '../../share/model/genre.model';
import { FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-admin-genre',
  templateUrl: './admin-genre.component.html',
  styleUrls: ['./admin-genre.component.css']
})
export class AdminGenreComponent implements OnInit {
  
  toUpdate:boolean=false;
  toListAll:boolean=true;
  toUpdateGenre:Genre;
  inputInfo:Genre=new Genre("","","");
  
  eventMessage:string="";
  genres:Genre[]=[];
  displayedColumns: string[] = ['id','name','description','actions'];
  updateForm:FormGroup;
  jwt=localStorage.getItem("jwt");
  
  constructor(private genreService:GenreService) { }
  
  ngOnInit() {
    this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
      console.log(this.genres);
    });
    
    this.updateForm = new FormGroup({
      'id'   : new FormControl(null, []),
      'name'   : new FormControl(null, [Validators.required, this.blankSpaces]),
      'description'  : new FormControl(null, [])
    });
  }
  
    blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }
  
  
   onUpdate(item:Genre){
    console.log("onUpdate: "+item);
    console.log("onUpdate: "+item.genre_id);
    console.log("onUpdate: "+item.genre_name);
    console.log("onUpdate: "+item.genre_description);
    
    this.toUpdateGenre=item;  
    this.inputInfo.genre_id=item.genre_id;
    this.inputInfo.genre_name=item.genre_name;
    this.inputInfo.genre_description=item.genre_description;
    
    this.toUpdate=true;
    this.eventMessage="";
  }
  
  onDelete(item:Genre){
    console.log(item);
    this.genreService.deleteGenre(item.genre_id.toString(),this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      this.eventMessage=result["message"];
      this.toUpdate=false;
      this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
      console.log(this.genres);
    });
    });
  }
  
  updateCancel(){
    this.toUpdate=false;
    this.eventMessage="Update Cancel";
    this.toUpdateGenre=new Genre("","","");
    this.inputInfo=new Genre("","","");
  }
  updateSubmit(){
    this.toUpdate=false;
    this.eventMessage="Update Submit";
    console.log(this.updateForm);
    
    console.log("updateSubmit: "+this.inputInfo.genre_id +this.updateForm.value.name +"a "+this.updateForm.value.description);
    
    
    this.inputInfo.genre_name=this.updateForm.value.name
    this.inputInfo.genre_description=this.updateForm.value.description;
      
    this.genreService.updateGenre(this.inputInfo,this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      this.eventMessage=result["message"];
      
      this.genreService.getGenres().subscribe(genres=>{
      this.genres=genres;
      console.log(this.genres);
    });
    });
  }
  
   onNewItemAdded(eventMessage:string){
    this.eventMessage=eventMessage;
    this.genreService.getGenres().subscribe(genres=>{
        this.genres=genres;
        this.toListAll=true;
        this.toUpdate=false;
      });
  }
  
  onNewItemAdding(eventMessage:string){
    this.eventMessage=eventMessage;
    this.toUpdate=false;
    this.toListAll=false;
  }
  
  onNewItemCancel(eventMessage:string){
    this.eventMessage=eventMessage;
    this.toUpdate=false;
    this.toListAll=true;
  }

}
