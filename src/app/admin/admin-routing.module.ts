import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login/login.component';
import { AdminGenreComponent} from './admin-genre/admin-genre.component';
import { AdminMovieComponent} from './admin-movie/admin-movie.component';
import { AdminShowtypeComponent} from './admin-showtype/admin-showtype.component';

import { GenreGetComponent} from './admin-genre/genre-get/genre-get.component';
import { MovieGetComponent} from './admin-movie/movie-get/movie-get.component';
import { ShowtypeGetComponent} from './admin-showtype/showtype-get/showtype-get.component';
import { GenreAddComponent} from './admin-genre/genre-add/genre-add.component';
import { MovieAddComponent} from './admin-movie/movie-add/movie-add.component';
import { ShowtypeAddComponent} from './admin-showtype/showtype-add/showtype-add.component';

import { MovieUpdateComponent} from './admin-movie/movie-update/movie-update.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'log-in',component:LoginComponent},
  {path:'movie',component:AdminMovieComponent},
  {path:'genre',component:AdminGenreComponent},
  {path:'showtype',component:AdminShowtypeComponent},
  {path:'genre/get',component:GenreGetComponent},
  {path:'movie/get',component:MovieGetComponent},
  {path:'showtype/get',component:ShowtypeGetComponent},
  {path:'genre/add',component:GenreAddComponent},
  {path:'movie/add', component:MovieAddComponent},
  {path:'showtype/add',component:ShowtypeAddComponent},
  {path:'movie/update',component:MovieUpdateComponent}
  
  
  ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
