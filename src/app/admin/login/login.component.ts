import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from '../../share/service/admin.service';
import { Admin } from '../../share/model/admin.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  @Output() login=new EventEmitter<string>();
  title:string="";
  eventMessage:string="";
  loginForm: FormGroup;
  toShowLoginForm:boolean;
  inputInfo:Admin=new Admin("","","","","");
  decode:string="";
  
  constructor(private adminService:AdminService) { }

  ngOnInit() {
    
    if (this.hasValidToken()){
      this.toShowLoginForm=false;
      this.adminService.isAuthenticated();
    }else{
      this.toShowLoginForm=true;
    }
    
    
    this.loginForm = new FormGroup({
      'name'   : new FormControl(null, [Validators.required]),
      'password'  : new FormControl(null, [Validators.required])
    });
  }
  
  //submit the form
    onSubmit(){
      console.log("component: "+this.loginForm)
      console.log("component: "+this.loginForm.value.name);
      console.log("component: "+this.loginForm.value.password);
      
      this.inputInfo.name=this.loginForm.value.name
      this.inputInfo.password=this.loginForm.value.password;
      
      console.log("component: "+this.inputInfo);
      this.adminService.login(this.inputInfo).subscribe(result=>{
        console.log("login "+result);
        console.log("login "+result["message"]);
        console.log("login "+result["result"]);
        
        if (result["result"]==="OK"){
          
          this.decode=this.adminService.getDecodedAccessToken(result["message"])
          console.log("component: "+this.decode);
          // localStorage.setItem("session", `{"exp":"${this.decode.exp}","name":"${this.decode.name}","role":"${this.decode.role}"}`);
        
          
          //message store the JWT
          //store the JWT into localstorage
          localStorage.setItem("jwt", result["message"]);
          this.eventMessage="login success";
          this.toShowLoginForm=false;
          this.adminService.isAuthenticated();
          
          
        }else if(result["result"]==="ERROR"){
          this.eventMessage=result["message"];
          
        }else{
          this.eventMessage="other error";
        }
        
        this.login.emit(this.eventMessage);
      });
    };
    
    
    //if token is valid, set admin.loggedIn to true
  //else clear local storage jwt and session, if token expired.
  
    hasValidToken(){

// exp: 1561243704
// iat: 1561157304
// name: "Alvin"
// role: "admin"

    var currentTime = Date.now() / 1000;
    console.log(currentTime);
    
    var validToken=false;
    var result:any;
    var jSession:any;
    var isAuthenticated=false;
    
    if ((localStorage.getItem("session") !== null)){
      var session=localStorage.getItem('session');
      console.log("\'"+session+"\'");
     
      jSession=JSON.parse(session);
      console.log("component "+jSession);
      console.log("component "+jSession.exp);
      console.log("component "+jSession.name);
      console.log("component "+jSession.role);
      
      if (Number(jSession.exp)>currentTime){
        isAuthenticated=true;
        this.adminService.isAuthenticated();
      }else{
        console.log("component: token expired: currentTime:" + currentTime.toString()+" exp:"+jSession.exp);
        //clear localstorage
        localStorage.setItem("session","");
      }
    }
    
    if ((localStorage.getItem("jwt") !== null)){
      var jwt=localStorage.getItem('jwt');
      result=this.adminService.getDecodedAccessToken(jwt);
      
      if (result.exp>currentTime){
        console.log("validToken");
        localStorage.setItem("session", `{"exp":"${result.exp}","name":"${result.name}","role":"${result.role}"}`);
        isAuthenticated=true;
        this.adminService.isAuthenticated();
      }else{
        console.log("token expired");
        localStorage.setItem("jwt","");
      }
    }else{
      console.log("no token");
    }
    
    return isAuthenticated;
  }
    
}
    


