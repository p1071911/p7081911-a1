import { Component, OnInit } from '@angular/core';
import { ShowtypeService } from '../../share/service/showtype.service';
import { Showtype } from '../../share/model/showtype.model';
import { FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-admin-showtype',
  templateUrl: './admin-showtype.component.html',
  styleUrls: ['./admin-showtype.component.css']
})
export class AdminShowtypeComponent implements OnInit {

  editForm:FormGroup;
  
  inputInfo:Showtype=new Showtype("","");
  toUpdate:boolean=false;
  toListAll:boolean=true;
  toUpdateShowtype:Showtype;
  eventMessage:string="";
  
  showtypes:Showtype[]=[];
  displayedColumns: string[] = ['id','name','actions'];
  jwt=localStorage.getItem("jwt");
  
  constructor(private showtypeService:ShowtypeService) { }

  ngOnInit() {
    this.showtypeService.getShowtypes().subscribe(showtypes=>{
      this.showtypes=showtypes;
      console.log(this.showtypes);
    });
    
    this.editForm=new FormGroup({
    'name':new FormControl(null,[Validators.required, this.blankSpaces])
  });
  
  }
  
  onUpdate(item:Showtype){
    this.eventMessage="";
    this.eventMessage="";
    
    console.log("onUpdate "+item.showtype_id +" "+item.showtype_name + " d"+ this.inputInfo.showtype_name);
    this.toUpdateShowtype=item;  
    this.inputInfo.showtype_id=item.showtype_id;
    this.toUpdate=true;
  }
  
  onDelete(item:Showtype){
    this.eventMessage="";
    console.log("onDelete"+ item.showtype_id);
    this.showtypeService.deleteShowtype(item.showtype_id.toString(),this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      this.eventMessage=result["message"];
      this.toUpdate=false;
      
      this.showtypeService.getShowtypes().subscribe(showtypes=>{
      this.showtypes=showtypes;
      console.log(this.showtypes);
    });
    });
  }
  
  updateCancel(){
    this.toUpdate=false;
    this.eventMessage="Update Cancel";
  }
  updateSubmit(){
    this.toUpdate=false;
    this.eventMessage="Update Submit";
    console.log(this.inputInfo.showtype_name +"a "+this.inputInfo.showtype_id);
    this.showtypeService.updateShowtype(this.inputInfo,this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      this.eventMessage=result["message"];
      
      this.showtypeService.getShowtypes().subscribe(showtypes=>{
      this.showtypes=showtypes;
      console.log(this.showtypes);
    });
    });
  }
  
  onNewItemAdded(eventMessage:string){
    this.eventMessage=eventMessage;
    this.showtypeService.getShowtypes().subscribe(showtypes=>{
        this.showtypes=showtypes;
        console.log(this.showtypes);
        this.toListAll=true;
        this.toUpdate=false;
      });
  }
  
  onNewItemAdding(eventMessage:string){
    this.eventMessage=eventMessage;
    this.toListAll=false;
    this.toUpdate=false;
  }
  
  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

}
