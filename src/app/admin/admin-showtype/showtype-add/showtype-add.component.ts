import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { ShowtypeService } from '../../../share/service/showtype.service';
import { Showtype } from '../../../share/model/showtype.model';
import { FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-showtype-add',
  templateUrl: './showtype-add.component.html',
  styleUrls: ['./showtype-add.component.css']
})
export class ShowtypeAddComponent implements OnInit {
  
 
  
  @Output() newItemAdded= new EventEmitter<string>();
  @Output() newItemAdding= new EventEmitter<string>();
  
  addForm:FormGroup;
  eventMessage:string=""; 
  inputInfo:Showtype=new Showtype("","");
  showtypes:Showtype[]=[];
  toAdd:boolean=false;
  toShowStatus:boolean=false;
  title:string="";
  jwt=localStorage.getItem("jwt");
  
  constructor(private showtypeService:ShowtypeService) { }

  ngOnInit() {
    this.title="Show Type";
    this.eventMessage="";
    
    
    this.addForm=new FormGroup({
    'name':new FormControl(null,[Validators.required])
  });
  
    
  }
  
  add(){
      this.toAdd=true;
      this.eventMessage="";
      this.toShowStatus=false;
      this.newItemAdding.emit("");
    }
    
  addSubmit(){
    console.log(this.inputInfo.showtype_name);
    console.log(this.inputInfo);
    
    this.showtypeService.addShowtype(this.inputInfo,this.jwt).subscribe(result=>{
      console.log(result);
      console.log(result["message"]);
      console.log(result["result"]);
      
      if (result["result"]==="OK"){
        this.eventMessage=result["message"];
      }else if(result["result"]==="ERROR"){
        this.eventMessage=result["message"];
      }else{
        this.eventMessage=result["message"];
      }
      this.newItemAdded.emit(this.eventMessage);
      
    });
    this.toAdd=false;
    this.toShowStatus=true;
  }
  
  addCancel(){
    this.toAdd=false;
    this.toShowStatus=false;
  }
}
