import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { AdminMovieComponent } from './admin-movie/admin-movie.component';
import { AdminGenreComponent } from './admin-genre/admin-genre.component';
import { GenreGetComponent } from './admin-genre/genre-get/genre-get.component';
import { MovieGetComponent } from './admin-movie/movie-get/movie-get.component';

import { MatButtonModule} from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { MatTabsModule} from '@angular/material/tabs';
import { MatTableModule} from '@angular/material/table';
import { MovieUpdateComponent } from './admin-movie/movie-update/movie-update.component';
import { AdminShowtypeComponent } from './admin-showtype/admin-showtype.component';
import { ShowtypeGetComponent } from './admin-showtype/showtype-get/showtype-get.component';
import { ShowtypeAddComponent } from './admin-showtype/showtype-add/showtype-add.component';
import { GenreAddComponent } from './admin-genre/genre-add/genre-add.component';
import { MovieAddComponent } from './admin-movie/movie-add/movie-add.component';

@NgModule({
  declarations: [LoginComponent, AdminMovieComponent, AdminGenreComponent, GenreGetComponent, MovieGetComponent, MovieUpdateComponent, AdminShowtypeComponent, ShowtypeGetComponent, ShowtypeAddComponent, GenreAddComponent, MovieAddComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule
  ]
})
export class AdminModule { }
